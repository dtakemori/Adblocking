#!/bin/bash

BLOCKLISTS="/var/lib/blocklists"
UNBOUND_ZONES="/etc/unbound/zones"

### Disconnect - tracking
if [ -f ${BLOCKLISTS}/disconnect_tracking.txt ] \
  && [ -r ${BLOCKLISTS}/disconnect_tracking.txt ] \
  && [ -s ${BLOCKLISTS}/disconnect_tracking.txt ] ; then

  /bin/egrep -v '^(#|$)' ${BLOCKLISTS}/disconnect_tracking.txt \
      | /usr/bin/sort -u \
      | /usr/bin/awk '{ print "local-zone: \"" $1 "\" always_nxdomain" }' \
      > ${UNBOUND_ZONES}/blacklist-disconnect_tracking.zone
fi


/usr/bin/sort -u \
      ${UNBOUND_ZONES}/blacklist-disconnect_tracking.zone \
      > ${UNBOUND_ZONES}/blacklist-tracking.zone


exit 0
