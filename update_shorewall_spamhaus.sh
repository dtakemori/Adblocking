#!/bin/bash

BLOCKLISTS="/var/lib/blocklists"

/bin/rm -f ${BLOCKLISTS}/spamhaus_drop.txt

/usr/bin/wget --quiet -O - \
	http://www.spamhaus.org/drop/drop.txt \
	> ${BLOCKLISTS}/spamhaus_drop.txt

if [ ${?} ] \
  && [ -f ${BLOCKLISTS}/spamhaus_drop.txt ] \
  && [ -r ${BLOCKLISTS}/spamhaus_drop.txt ] \
  && [ -s ${BLOCKLISTS}/spamhaus_drop.txt ] ; then

  for n in $(/bin/egrep -v '^;' ${BLOCKLISTS}/spamhaus_drop.txt | /bin/sed 's/;.*//'); do
    /usr/sbin/ipset -exist add SW_DBL4 ${n} timeout 129600;
  done
fi
