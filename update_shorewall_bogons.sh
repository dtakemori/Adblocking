#!/bin/bash

BLOCKLISTS="/var/lib/blocklists"

/bin/rm -f ${BLOCKLISTS}/fullbogons-ipv4.txt

/usr/bin/wget --quiet -O - \
	https://www.team-cymru.org/Services/Bogons/fullbogons-ipv4.txt \
	> ${BLOCKLISTS}/fullbogons-ipv4.txt

if [ ${?} ] \
  && [ -f ${BLOCKLISTS}/fullbogons-ipv4.txt ] \
  && [ -r ${BLOCKLISTS}/fullbogons-ipv4.txt ] \
  && [ -s ${BLOCKLISTS}/fullbogons-ipv4.txt ] ; then

  ### Strip out the private IP ranges in use in the network as necessary
  for n in $(/bin/egrep -v '^(#|0\.0\.0\.0/8|10\.0\.0\.0/8|127\.0\.0\.0/8|172\.16\.0\.0/12|192\.168\.0\.0/16)' ${BLOCKLISTS}/fullbogons-ipv4.txt); do
    /usr/sbin/ipset -exist add SW_DBL4 ${n} timeout 129600;
  done
fi
