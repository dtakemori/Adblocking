#!/bin/bash

BLOCKLISTS="/var/lib/blocklists"
SQUID_ACLS="/etc/squid/ACLS"

### Adguard English
if [ -f ${BLOCKLISTS}/adguard_english.txt ] \
  && [ -r ${BLOCKLISTS}/adguard_english.txt ] \
  && [ -s ${BLOCKLISTS}/adguard_english.txt ] ; then

  ### Only rules of the form ||domain^
  /bin/egrep '^\|\|[^/]*\^$' ${BLOCKLISTS}/adguard_english.txt \
      | /bin/sed 's/^||\(.*\)\^$/\1/' \
      | /usr/bin/sort -u \
      > ${SQUID_ACLS}/domains-adguard_english.txt
fi


### Disconnect Ads
if [ -f ${BLOCKLISTS}/disconnect_ads.txt ] \
  && [ -r ${BLOCKLISTS}/disconnect_ads.txt ] \
  && [ -s ${BLOCKLISTS}/disconnect_ads.txt ] ; then

  /bin/egrep -v '^$' ${BLOCKLISTS}/disconnect_ads.txt \
    | /usr/bin/sort -u \
    > ${SQUID_ACLS}/domains-disconnect_ads.txt
fi


### Easylist
if [ -f ${BLOCKLISTS}/easylist.txt ] \
  && [ -r ${BLOCKLISTS}/easylist.txt ] \
  && [ -s ${BLOCKLISTS}/easylist.txt ] ; then

  ### Only rules of the form ||domain^
  /bin/egrep '^\|\|[^/]*\^$' ${BLOCKLISTS}/easylist.txt \
      | /bin/sed 's/^||\(.*\)\^$/\1/' \
      | /usr/bin/sort -u \
      > ${SQUID_ACLS}/domains-easylist.txt
fi


### Peter Lowe's adserver list
if [ -f ${BLOCKLISTS}/yoyo-adservers.txt ] \
  && [ -r ${BLOCKLISTS}/yoyo-adservers.txt ] \
  && [ -s ${BLOCKLISTS}/yoyo-adservers.txt ]; then

  /bin/cp -f ${BLOCKLISTS}/yoyo-adservers.txt \
             ${SQUID_ACLS}/domains-yoyo_adservers.txt
fi

exit 0
