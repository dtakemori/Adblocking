#!/bin/bash

BLOCKLISTS="/var/lib/blocklists"
UNBOUND_ZONES="/etc/unbound/zones"

### Disconnect Ads
if [ -f ${BLOCKLISTS}/disconnect_ads.txt ] \
  && [ -r ${BLOCKLISTS}/disconnect_ads.txt ] \
  && [ -s ${BLOCKLISTS}/disconnect_ads.txt ] ; then

  /bin/egrep -v '^$' ${BLOCKLISTS}/disconnect_ads.txt \
    | /usr/bin/sort -u \
    | /usr/bin/awk '{ print "local-zone: \"" $1 "\" always_nxdomain" }' \
    > ${UNBOUND_ZONES}/blacklist-disconnect_ads.zone
fi


### Peter Lowe's adserver list
if [ -f ${BLOCKLISTS}/yoyo-adservers.txt ] \
  && [ -r ${BLOCKLISTS}/yoyo-adservers.txt ] \
  && [ -s ${BLOCKLISTS}/yoyo-adservers.txt ]; then

  /bin/egrep -v '^$' ${BLOCKLISTS}/yoyo-adservers.txt \
    | /usr/bin/sort -u \
    | /usr/bin/awk '{ print "local-zone: \"" $1 "\" always_nxdomain" }' \
    > ${UNBOUND_ZONES}/blacklist-yoyo_adservers.zone
fi


/usr/bin/sort -u \
    ${UNBOUND_ZONES}/blacklist-disconnect_ads.zone \
    ${UNBOUND_ZONES}/blacklist-yoyo_adservers.zone \
    > ${UNBOUND_ZONES}/blacklist-ads.zone


exit 0
