#!/bin/bash

BLOCKLISTS="/var/lib/blocklists"

### Adguard English list
(/bin/rm -f ${BLOCKLISTS}/adguard_english.txt-new
/usr/bin/wget --quiet --no-check-certificate -O - \
    "https://adguard.com/en/filter-rules.html?id=2" \
    | /bin/sed 's/\r$//' \
    > ${BLOCKLISTS}/adguard_english.txt-new

if [ ${PIPESTATUS[0]} ] \
  && [ -f ${BLOCKLISTS}/adguard_english.txt-new ] \
  && [ -r ${BLOCKLISTS}/adguard_english.txt-new ] \
  && [ -s ${BLOCKLISTS}/adguard_english.txt-new ] ; then

    /bin/mv -f ${BLOCKLISTS}/adguard_english.txt-new \
               ${BLOCKLISTS}/adguard_english.txt
fi) &


### Adguard Spyware list
(/bin/rm -f ${BLOCKLISTS}/adguard_spyware.txt-new
/usr/bin/wget --quiet --no-check-certificate -O - \
    "https://adguard.com/en/filter-rules.html?id=3" \
    | /bin/sed 's/\r$//' \
    > ${BLOCKLISTS}/adguard_spyware.txt-new

if [ ${PIPESTATUS[0]} ] \
  && [ -f ${BLOCKLISTS}/adguard_spyware.txt-new ] \
  && [ -r ${BLOCKLISTS}/adguard_spyware.txt-new ] \
  && [ -s ${BLOCKLISTS}/adguard_spyware.txt-new ] ; then

    /bin/mv -f ${BLOCKLISTS}/adguard_spyware.txt-new \
               ${BLOCKLISTS}/adguard_spyware.txt
fi) &


### Easylist AntiAdBlock list
(/bin/rm -f ${BLOCKLISTS}/antiadblockfilters.txt-new
/usr/bin/wget --quiet --no-check-certificate -O - \
    https://easylist-downloads.adblockplus.org/antiadblockfilters.txt \
    > ${BLOCKLISTS}/antiadblockfilters.txt-new

if [ ${PIPESTATUS[0]} ] \
  && [ -f ${BLOCKLISTS}/antiadblockfilters.txt-new ] \
  && [ -r ${BLOCKLISTS}/antiadblockfilters.txt-new ] \
  && [ -s ${BLOCKLISTS}/antiadblockfilters.txt-new ] ; then

    /bin/mv -f ${BLOCKLISTS}/antiadblockfilters.txt-new \
               ${BLOCKLISTS}/antiadblockfilters.txt
fi) &


### Disconnect Advertising list
(/bin/rm -f ${BLOCKLISTS}/disconnect_ads.txt-new
/usr/bin/wget --quiet --no-check-certificate -O - \
    https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt \
    > ${BLOCKLISTS}/disconnect_ads.txt-new

if [ ${PIPESTATUS[0]} ] \
  && [ -f ${BLOCKLISTS}/disconnect_ads.txt-new ] \
  && [ -r ${BLOCKLISTS}/disconnect_ads.txt-new ] \
  && [ -s ${BLOCKLISTS}/disconnect_ads.txt-new ] ; then

    /bin/mv -f ${BLOCKLISTS}/disconnect_ads.txt-new \
               ${BLOCKLISTS}/disconnect_ads.txt
fi) &


### Disconnect Malvertising list
(/bin/rm -f ${BLOCKLISTS}/disconnect_malvertising.txt-new
/usr/bin/wget --quiet --no-check-certificate -O - \
    https://s3.amazonaws.com/lists.disconnect.me/simple_malvertising.txt \
    > ${BLOCKLISTS}/disconnect_malvertising.txt-new

if [ ${PIPESTATUS[0]} ] \
  && [ -f ${BLOCKLISTS}/disconnect_malvertising.txt-new ] \
  && [ -r ${BLOCKLISTS}/disconnect_malvertising.txt-new ] \
  && [ -s ${BLOCKLISTS}/disconnect_malvertising.txt-new ] ; then

    /bin/mv -f ${BLOCKLISTS}/disconnect_malvertising.txt-new \
               ${BLOCKLISTS}/disconnect_malvertising.txt
fi) &


### Disconnect.me - simple malware list
(/bin/rm -f ${BLOCKLISTS}/disconnect_malware.txt-new
/usr/bin/wget --quiet --no-check-certificate -O - \
    https://s3.amazonaws.com/lists.disconnect.me/simple_malware.txt \
    > ${BLOCKLISTS}/disconnect_malware.txt-new

if [ ${PIPESTATUS[0]} ] \
  && [ -f ${BLOCKLISTS}/disconnect_malware.txt-new ] \
  && [ -r ${BLOCKLISTS}/disconnect_malware.txt-new ] \
  && [ -s ${BLOCKLISTS}/disconnect_malware.txt-new ] ; then

    /bin/mv -f ${BLOCKLISTS}/disconnect_malware.txt-new \
               ${BLOCKLISTS}/disconnect_malware.txt
fi) &


### Disconnect Tracking list
(/bin/rm -f ${BLOCKLISTS}/disconnect_tracking.txt-new
/usr/bin/wget --quiet --no-check-certificate -O - \
    https://s3.amazonaws.com/lists.disconnect.me/simple_tracking.txt \
    > ${BLOCKLISTS}/disconnect_tracking.txt-new

if [ ${PIPESTATUS[0]} ] \
  && [ -f ${BLOCKLISTS}/disconnect_tracking.txt-new ] \
  && [ -r ${BLOCKLISTS}/disconnect_tracking.txt-new ] \
  && [ -s ${BLOCKLISTS}/disconnect_tracking.txt-new ] ; then

    /bin/mv -f ${BLOCKLISTS}/disconnect_tracking.txt-new \
               ${BLOCKLISTS}/disconnect_tracking.txt
fi) &


### EasyList
(/bin/rm -f ${BLOCKLISTS}/easylist.txt-new
/usr/bin/wget --quiet --no-check-certificate -O - \
    https://easylist-downloads.adblockplus.org/easylist_noelemhide.txt \
    > ${BLOCKLISTS}/easylist.txt-new

if [ ${PIPESTATUS[0]} ] \
  && [ -f ${BLOCKLISTS}/easylist.txt-new ] \
  && [ -r ${BLOCKLISTS}/easylist.txt-new ] \
  && [ -s ${BLOCKLISTS}/easylist.txt-new ] ; then

    /bin/mv -f ${BLOCKLISTS}/easylist.txt-new \
               ${BLOCKLISTS}/easylist.txt
fi) &


### EasyPrivacy
(/bin/rm -f ${BLOCKLISTS}/easyprivacy.txt-new
/usr/bin/wget --quiet --no-check-certificate -O - \
    https://easylist-downloads.adblockplus.org/easyprivacy.txt \
    > ${BLOCKLISTS}/easyprivacy.txt-new

if [ ${PIPESTATUS[0]} ] \
  && [ -f ${BLOCKLISTS}/easyprivacy.txt-new ] \
  && [ -r ${BLOCKLISTS}/easyprivacy.txt-new ] \
  && [ -s ${BLOCKLISTS}/easyprivacy.txt-new ] ; then

    /bin/mv -f ${BLOCKLISTS}/easyprivacy.txt-new \
               ${BLOCKLISTS}/easyprivacy.txt
fi) &


### Fanboys Annoyances
(/bin/rm -f ${BLOCKLISTS}/fanboy-annoyance.txt-new
/usr/bin/wget --quiet --no-check-certificate -O - \
    https://easylist.to/easylist/fanboy-annoyance.txt \
    > ${BLOCKLISTS}/fanboy-annoyance.txt-new

if [ ${PIPESTATUS[0]} ] \
  && [ -f ${BLOCKLISTS}/fanboy-annoyance.txt-new ] \
  && [ -r ${BLOCKLISTS}/fanboy-annoyance.txt-new ] \
  && [ -s ${BLOCKLISTS}/fanboy-annoyance.txt-new ] ; then

    /bin/mv -f ${BLOCKLISTS}/fanboy-annoyance.txt-new \
               ${BLOCKLISTS}/fanboy-annoyance.txt
fi) &


### Fanboys Enhanced
(/bin/rm -f ${BLOCKLISTS}/fanboy-enhancedstats.txt-new
/usr/bin/wget --quiet --no-check-certificate -O - \
    https://fanboy.co.nz/enhancedstats.txt \
    > ${BLOCKLISTS}/fanboy-enhancedstats.txt-new

if [ ${PIPESTATUS[0]} ] \
  && [ -f ${BLOCKLISTS}/fanboy-enhancedstats.txt-new ] \
  && [ -r ${BLOCKLISTS}/fanboy-enhancedstats.txt-new ] \
  && [ -s ${BLOCKLISTS}/fanboy-enhancedstats.txt-new ] ; then

    /bin/mv -f ${BLOCKLISTS}/fanboy-enhancedstats.txt-new \
               ${BLOCKLISTS}/fanboy-enhancedstats.txt
fi) &


### Hosh Sadiq's browser based crypto miner blocklist
(/bin/rm -f ${BLOCKLISTS}/nocoin_hosts.txt-new 
/usr/bin/wget --quiet --no-check-certificate -O - \
    https://raw.githubusercontent.com/hoshsadiq/adblock-nocoin-list/master/hosts.txt \
    > ${BLOCKLISTS}/nocoin_hosts.txt-new

if [ ${PIPESTATUS[0]} ] \
  && [ -f ${BLOCKLISTS}/nocoin_hosts.txt-new ] \
  && [ -r ${BLOCKLISTS}/nocoin_hosts.txt-new ] \
  && [ -s ${BLOCKLISTS}/nocoin_hosts.txt-new ] ; then

    /bin/mv -f ${BLOCKLISTS}/nocoin_hosts.txt-new \
               ${BLOCKLISTS}/nocoin_hosts.txt
fi) &


### Dan Pollock's "someonewhocares" hosts block list
(/bin/rm -f ${BLOCKLISTS}/someonewhocares.txt-new
/usr/bin/wget --quiet --no-check-certificate -O - \
    http://someonewhocares.org/hosts/hosts \
    > ${BLOCKLISTS}/someonewhocares.txt-new
 
if [ ${PIPESTATUS[0]} ] \
  && [ -f ${BLOCKLISTS}/someonewhocares.txt-new ] \
  && [ -r ${BLOCKLISTS}/someonewhocares.txt-new ] \
  && [ -s ${BLOCKLISTS}/someonewhocares.txt-new ] ; then

    /bin/mv -f ${BLOCKLISTS}/someonewhocares.txt-new \
               ${BLOCKLISTS}/someonewhocares.txt
fi) &


### Ublock Origin
(/bin/rm -f ${BLOCKLISTS}/ublock-filters.txt-new
/usr/bin/wget --quiet --no-check-certificate -O - \
    "https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/filters.txt" \
    > ${BLOCKLISTS}/ublock-filters.txt

if [ ${PIPESTATUS[0]} ] \
  && [ -f ${BLOCKLISTS}/ublock-filters.txt-new ] \
  && [ -r ${BLOCKLISTS}/ublock-filters.txt-new ] \
  && [ -s ${BLOCKLISTS}/ublock-filters.txt-new ] ; then

   /bin/mv -f ${BLOCKLISTS}/ublock-filters.txt-new \
              ${BLOCKLISTS}/ublock-filters.txt
fi) &

(/bin/rm -f ${BLOCKLISTS}/ublock-badware.txt-new
/usr/bin/wget --quiet --no-check-certificate -O - \
    "https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/badware.txt" \
    > ${BLOCKLISTS}/ublock-badware.txt

if [ ${PIPESTATUS[0]} ] \
  && [ -f ${BLOCKLISTS}/ublock-badware.txt-new ] \
  && [ -r ${BLOCKLISTS}/ublock-badware.txt-new ] \
  && [ -s ${BLOCKLISTS}/ublock-badware.txt-new ] ; then

   /bin/mv -f ${BLOCKLISTS}/ublock-badware.txt-new \
              ${BLOCKLISTS}/ublock-badware.txt
fi) &

(/bin/rm -f ${BLOCKLISTS}/ublock-privacy.txt-new
/usr/bin/wget --quiet --no-check-certificate -O - \
    "https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/privacy.txt" \
    > ${BLOCKLISTS}/ublock-privacy.txt

if [ ${PIPESTATUS[0]} ] \
  && [ -f ${BLOCKLISTS}/ublock-privacy.txt-new ] \
  && [ -r ${BLOCKLISTS}/ublock-privacy.txt-new ] \
  && [ -s ${BLOCKLISTS}/ublock-privacy.txt-new ] ; then

   /bin/mv -f ${BLOCKLISTS}/ublock-privacy.txt-new \
              ${BLOCKLISTS}/ublock-privacy.txt
fi) &

(/bin/rm -f ${BLOCKLISTS}/ublock-resource-abuse.txt-new
/usr/bin/wget --quiet --no-check-certificate -O - \
    "https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/resource-abuse.txt" \
    > ${BLOCKLISTS}/ublock-resource-abuse.txt

if [ ${PIPESTATUS[0]} ] \
  && [ -f ${BLOCKLISTS}/ublock-resource-abuse.txt-new ] \
  && [ -r ${BLOCKLISTS}/ublock-resource-abuse.txt-new ] \
  && [ -s ${BLOCKLISTS}/ublock-resource-abuse.txt-new ] ; then

   /bin/mv -f ${BLOCKLISTS}/ublock-resource-abuse.txt-new \
              ${BLOCKLISTS}/ublock-resource-abuse.txt
fi) &

(/bin/rm -f ${BLOCKLISTS}/ublock-unbreak.txt-new
/usr/bin/wget --quiet --no-check-certificate -O - \
    "https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/unbreak.txt" \
    > ${BLOCKLISTS}/ublock-unbreak.txt

if [ ${PIPESTATUS[0]} ] \
  && [ -f ${BLOCKLISTS}/ublock-unbreak.txt-new ] \
  && [ -r ${BLOCKLISTS}/ublock-unbreak.txt-new ] \
  && [ -s ${BLOCKLISTS}/ublock-unbreak.txt-new ] ; then

   /bin/mv -f ${BLOCKLISTS}/ublock-unbreak.txt-new \
              ${BLOCKLISTS}/ublock-unbreak.txt
fi) &


### Winhelp2002
(/bin/rm -f ${BLOCKLISTS}/winhelp202.txt-new
/usr/bin/wget --quiet --no-check-certificate -O - \
    http://winhelp2002.mvps.org/hosts.txt \
    > ${BLOCKLISTS}/winhelp202.txt-new

if [ ${PIPESTATUS[0]} ] \
  && [ -f ${BLOCKLISTS}/winhelp202.txt-new ] \
  && [ -r ${BLOCKLISTS}/winhelp202.txt-new ] \
  && [ -s ${BLOCKLISTS}/winhelp202.txt-new ] ; then

   /bin/mv -f ${BLOCKLISTS}/winhelp202.txt-new \
              ${BLOCKLISTS}/winhelp202.txt
fi) &


### Peter Lowe's adserver list
(/bin/rm -f ${BLOCKLISTS}/yoyo-adservers.txt-new
/usr/bin/wget --quiet --no-check-certificate -O - \
    "https://pgl.yoyo.org/as/serverlist.php?hostformat=nohtml&showintro=1" \
    > ${BLOCKLISTS}/yoyo-adservers.txt-new

if [ ${PIPESTATUS[0]} ] \
  && [ -f ${BLOCKLISTS}/yoyo-adservers.txt-new ] \
  && [ -r ${BLOCKLISTS}/yoyo-adservers.txt-new ] \
  && [ -s ${BLOCKLISTS}/yoyo-adservers.txt-new ] ; then

   /bin/mv -f ${BLOCKLISTS}/yoyo-adservers.txt-new \
              ${BLOCKLISTS}/yoyo-adservers.txt
fi) &


### Mitchell Krog's The-Big-List-of-Hacked-Malware-Web-Sites
(/bin/rm -f ${BLOCKLISTS}/tblohmws.txt-new
/usr/bin/wget --quiet --no-check-certificate -O - \
    "https://raw.githubusercontent.com/mitchellkrogza/The-Big-List-of-Hacked-Malware-Web-Sites/master/.dev-tools/output/domains/ACTIVE/list" \
    > ${BLOCKLISTS}/tblohmws.txt-new

if [ ${PIPESTATUS[0]} ] \
  && [ -f ${BLOCKLISTS}/tblohmws.txt-new ] \
  && [ -r ${BLOCKLISTS}/tblohmws.txt-new ] \
  && [ -s ${BLOCKLISTS}/tblohmws.txt-new ] ; then

   /bin/mv -f ${BLOCKLISTS}/tblohmws.txt-new \
              ${BLOCKLISTS}/tblohmws.txt
fi) &


### Phishing Army
(/bin/rm -f ${BLOCKLISTS}/phishing_army_blocklist.txt-new
/usr/bin/wget --quiet --no-check-certificate -O - \
    "https://phishing.army/download/phishing_army_blocklist.txt" \
    > ${BLOCKLISTS}/phishing_army_blocklist.txt-new

if [ ${PIPESTATUS[0]} ] \
  && [ -f ${BLOCKLISTS}/phishing_army_blocklist.txt-new ] \
  && [ -r ${BLOCKLISTS}/phishing_army_blocklist.txt-new ] \
  && [ -s ${BLOCKLISTS}/phishing_army_blocklist.txt-new ] ; then

   /bin/mv -f ${BLOCKLISTS}/phishing_army_blocklist.txt-new \
              ${BLOCKLISTS}/phishing_army_blocklist.txt
fi) &


wait
exit 0
