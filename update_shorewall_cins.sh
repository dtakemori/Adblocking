#!/bin/bash

BLOCKLISTS="/var/lib/blocklists"

/bin/rm -f ${BLOCKLISTS}/cins-army.txt

/usr/bin/wget --quiet -O - \
	http://cinsscore.com/list/ci-badguys.txt \
	> ${BLOCKLISTS}/cins-army.txt

if [ ${?} ] \
  && [ -f ${BLOCKLISTS}/cins-army.txt ] \
  && [ -r ${BLOCKLISTS}/cins-army.txt ] \
  && [ -s ${BLOCKLISTS}/cins-army.txt ] ; then

  for i in $(cat ${BLOCKLISTS}/cins-army.txt); do
    /usr/sbin/ipset -exist add SW_DBL4 ${i} timeout 43200;
  done
fi
