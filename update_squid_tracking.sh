#!/bin/bash

BLOCKLISTS="/var/lib/blocklists"
SQUID_ACLS="/etc/squid/ACLS"

### Disconnect - tracking
if [ -f ${BLOCKLISTS}/disconnect_tracking.txt ] \
  && [ -r ${BLOCKLISTS}/disconnect_tracking.txt ] \
  && [ -s ${BLOCKLISTS}/disconnect_tracking.txt ] ; then

  /bin/egrep -v '^(#|$)' ${BLOCKLISTS}/disconnect_tracking.txt \
      | /usr/bin/sort -u \
      > ${SQUID_ACLS}/domains-disconnect_tracking.txt
fi


### Easyprivacy
if [ -f ${BLOCKLISTS}/easyprivacy.txt ] \
  && [ -r ${BLOCKLISTS}/easyprivacy.txt ] \
  && [ -s ${BLOCKLISTS}/easyprivacy.txt ] ; then

  ### Only rules of the form ||domain^
  /bin/egrep '^\|\|[^/]*\^$' ${BLOCKLISTS}/easyprivacy.txt \
      | /bin/sed 's/^||\(.*\)\^$/\1/' \
      | /usr/bin/sort -u \
      > ${SQUID_ACLS}/domains-easyprivacy.txt
fi


exit 0
