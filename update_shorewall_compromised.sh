#!/bin/bash

BLOCKLISTS="/var/lib/blocklists"

/bin/rm -f ${BLOCKLISTS}/compromised-ips.txt

/usr/bin/wget --quiet -O - \
	https://rules.emergingthreats.net/blockrules/compromised-ips.txt \
	> ${BLOCKLISTS}/compromised-ips.txt

if [ ${?} ] \
  && [ -f ${BLOCKLISTS}/compromised-ips.txt ] \
  && [ -r ${BLOCKLISTS}/compromised-ips.txt ] \
  && [ -s ${BLOCKLISTS}/compromised-ips.txt ] ; then

  for i in $(cat ${BLOCKLISTS}/compromised-ips.txt); do
    /usr/sbin/ipset -exist add SW_DBL4 ${i} timeout 43200;
  done
fi
