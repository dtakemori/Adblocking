#!/bin/bash

BLOCKLISTS="/var/lib/blocklists"

/bin/rm -f ${BLOCKLISTS}/dshield.txt

/usr/bin/wget --quiet -O - \
	https://feeds.dshield.org/block.txt \
	> ${BLOCKLISTS}/dshield.txt

if [ ${?} ] \
  && [ -f ${BLOCKLISTS}/dshield.txt ] \
  && [ -r ${BLOCKLISTS}/dshield.txt ] \
  && [ -s ${BLOCKLISTS}/dshield.txt ] ; then

  for n in $(/bin/egrep -v '^(#|Start)' ${BLOCKLISTS}/dshield.txt | /usr/bin/awk '{ print $1 "/" $3 }'); do
    /usr/sbin/ipset -exist add SW_DBL4 ${n} timeout 43200;
  done
fi
